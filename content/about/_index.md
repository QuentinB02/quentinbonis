---
date: "2018-07-12T18:19:33+06:00"
desc_first: Having focused my college education at <a href="https://www.isae-supaero.fr/fr/">ISAE-Supaero</a> on applied machine learning, I spent two years as a Geo-Information Data Scientist for <a href="https://www.telespazio.fr/fr">Telespazio France</a>, with a strong emphasis on deep-learning research (especially satellite imagery processing) and data visualization routines. See below for a few projects I worked on at the time. 

desc_second: <br/>I then created a consulting company with two colleagues, <a href="https://fdti-consulting.com/">FDTI Consulting</a>, to apply and further extend my knowledge, while pursuing my passions. Check out my portfolio to see examples of projects I worked on in this context. 

desc_three: I now work as a freelance data scientist. For more details check out my <a href="../portfolio/resume.pdf">resume </a>or my <a href="https://www.linkedin.com/in/quentin-bonis-7b798112a/">LinkedIn profile.</a> </br> Over the years I also built a small graphic design portfolio on <a href="https://www.behance.net/quentin_bonis">Behance</a>, feel free to check it out and leave a comment!

cise_desc: I worked for two years on implementing a Data Mining System for the Common Information Sharing Environment funded by the European Commission (CISE, cf the website of <a href="http://eucisedms.com/">CISE-DMS</a>). One of the aims of the system is to use AIS data (Automatic Identification System) to extract maritime routes, in order to define patterns of life standards and eventually predict vessel movements. Here is an example of density-based clustering on a large amount of AIS data points. 

image_desc: Another aspect of my job as a data scientist at Telespazio was to process satellite imagery in order to extract ship positions for business-oriented purposes. Typically, I would process such an image by applying a convolutional neural network (trained on operator-processed images), then an SVM classifier to set apart the interesting detections from the noise (for example in this case rocks or wave crests).

expertise_sectors:
- Python / R / SQL
- Database Management
- API Integration
- Geospatial Data
- Applied Machine Learning
- Computer Vision
- Natural Language Processing
- Time Series Forecasting
- Web Design
- Dynamic Applications
- Live Reporting
- Web Scraping
- Graphic Design
expertise_title: Expertise
heading: I work as a freelance data scientist to meet any data needs you may have
title: Quentin BONIS || About Me
---
