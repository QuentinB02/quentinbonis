---
category:
- Applied Machine Learning
date: "2019-12-23T20:56:42+06:00"
image: images/projects/fraud.jpg
project_images:
- images/projects/project-details-image-one.jpg
- images/projects/project-details-image-two.jpg
title: Fraud Detection on Financial Transactions
type: portfolio
custom_url: fraud_detection.html
---
